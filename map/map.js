let map;
let userLocation = null;
let markers = [];

function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 4.681435, lng: -74.038624 },
        zoom: 14,
        styles: [
            {
                featureType: "poi",
                stylers: [{ visibility: "off" }]
            },
            {
                featureType: "administrative",
                elementType: "labels",
                stylers: [{ visibility: "off" }]
            }
        ]
    });

    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            let pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            map.setCenter(pos);
        }, function () {
            // Geolocation is not allowed
        });
    } else {
        // Browser doesn't support Geolocation
    }
    doFetch()
    setInterval(doFetch, 10000)
}

function doFetch() {
    if (userLocation) {
        userLocation.setMap(null);
    }
    showCurrentLocation();

    markers.map(marker => {
        marker.setMap(null);
    });
    fetch("https://nebus.xyz/api/recent-thesis-locations")
        .then(resp => resp.json())
        .then(response => {
            showMarkers(response);
        });
}

function showCurrentLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            userLocation = null;
            let pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            const icon = {
                path: "M 0, 0m -7, 0a 7,7 0 1,0 14,0a 7,7 0 1,0 -14,0",
                fillColor: "#4285F4",
                fillOpacity: 1,
                strokeColor: "#FFFFFF",
                scale: 1
            };

            userLocation = new google.maps.Marker({
                position: pos,
                map: map,
                title: 'Tu ubicación',
                icon: icon
            });
        }, function () {
            // Geolocation is not allowed
        });
    }
}

function showMarkers(locations) {
    markers = [];
    locations.locations.map(bus => {
        let busRoute = "";
        if (bus.course !== 0 && 0 < bus.speed) {
            if (90 < bus.course && bus.course < 270) {
                busRoute = "Germania";
            } else {
                busRoute = "Lijacá";
            }
        }
        const dt = moment(bus.ts, "YYYY-MM-DD HH:mm:ss");
        const now = moment();
        const secondsSince = Math.max(Math.floor((now - dt)/1000), 0);
        const minutesSince = Math.floor(secondsSince/60);
        const speed = Math.floor(bus.speed*3.6);
        const info = `${busRoute}: Hace ${minutesSince} min, ${speed} km/h`.trim();
        const fillOpacity = Math.max(Math.min(1 - secondsSince/250, 1), 0);
        const icon = {
            path: "M0,0 L0,27 A3,3 0 0,0 3,30 L7,30 A3,3 0 0,0 10,27 L10,0 Z",
            fillColor: "#FA9543",
            fillOpacity: fillOpacity,
            strokeColor: "#F56207",
            scale: 1,
            rotation: bus.course-180
        };

        const marker = new google.maps.Marker({
            position: { lat: parseFloat(bus.latitude), lng: parseFloat(bus.longitude) },
            map: map,
            title: 'Bus',
            icon: icon
        });
        let infowindow = new google.maps.InfoWindow({
            content: info
        });

        markers.push(marker);
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
    })
}
